
Helper to assemble Horse64 SDK download
=======================================

This is a meta repo with a helper script for a pre-assembled
download of [Horse64](https://horse64.org) and [Moose64
](https://m64.horse64.org) core tooling.

This is provided as a convenience **for developers.**


Legal notices / licensing
-------------------------

It's [free software (other than logo branding), see
here](LICENSE.md). Check [the security notice](SECURITY%20NOTICE.md).


There's a warning installing `helpers-for-sdk-download.horse64.org` with horp?
------------------------------------------------------------------------------

**Don't install this package with horp,** just install
`core.horse64.org` itself and whatever else you want.
This package is **only** for users without horp already.


