**IMPORTANT NOTE:** These pre-compiled free binaries for Horse64 and
Moose64 are **only for developers,** and **aren't** meant for end-users,
and **aren't** security-assessed, and **aren't** guaranteed to get patches.
**There's no warranty, see [license](LICENSE.md).**
**This is an unpaid noncommercial open-source project run by volunteers.**

